package com.company.graph2;

import javafx.util.Pair;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

public class KruskalAlgorithm {
    public Integer goKruskal(int[][] adjacencyMatrix) {
        ArrayList<int[]> edges = new ArrayList<>();
        for (int i = 0; i < adjacencyMatrix.length; i++) {
            for (int j = i + 1; j < adjacencyMatrix.length; j++) {
                if (adjacencyMatrix[i][j] > 0) {
                    int[] paramEdge = new int[3];
                    paramEdge[1] = i;
                    paramEdge[2] = j;
                    paramEdge[0] = adjacencyMatrix[i][j];
                    edges.add(paramEdge);
                }
            }
        }
        sortEdges(edges);
        int[] numNods = new int[adjacencyMatrix.length];
        for (int i = 0; i < numNods.length; i++) {
            numNods[i] = i;
        }
        int sum = 0;
        for (int[] edge : edges) {
            int weight = edge[0];
            int start = edge[1];
            int end = edge[2];
            if (numNods[start] != numNods[end]) {
                sum += weight;
                int a = numNods[start];
                int b = numNods[end];
                for (int i = 0; i < numNods.length; i++) {
                    if (numNods[i] == b) {
                        numNods[i] = a;
                    }
                }
            }
        }
        return sum;
    }

    public void sortEdges(ArrayList<int[]> edges) {
        for (int i = 0; i < edges.size(); i++) {
            for (int j = i + 1; j < edges.size(); j++) {
                if (edges.get(i)[0] > edges.get(j)[0]) {
                    int[] t = edges.get(i);
                    edges.set(i, edges.get(j));
                    edges.set(j, t);
                }
            }
        }
    }
}
