package com.company.graph2;

import java.util.HashMap;

public class GraphTasks2Solution implements GraphTasks2 {
    @Override
    public HashMap<Integer, Integer> dijkstraSearch(int[][] adjacencyMatrix, int startIndex) {
        AlgorithmDijkstra algorithmDijkstra = new AlgorithmDijkstra();
        return algorithmDijkstra.goDijkstra(adjacencyMatrix, startIndex);
    }

    @Override
    public Integer primaAlgorithm(int[][] adjacencyMatrix) {
        AlgorithmPrima algorithmPrima = new AlgorithmPrima();
        return algorithmPrima.goPrima(adjacencyMatrix);
    }

    @Override
    public Integer kraskalAlgorithm(int[][] adjacencyMatrix) {
        KruskalAlgorithm kruskalAlgorithm = new KruskalAlgorithm();
        return kruskalAlgorithm.goKruskal(adjacencyMatrix);
    }
}
