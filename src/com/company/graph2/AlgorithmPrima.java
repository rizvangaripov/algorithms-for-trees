package com.company.graph2;

import java.util.HashMap;
import java.util.HashSet;

public class AlgorithmPrima {

    public Integer goPrima (int[][] adjacencyMatrix) {
        HashMap<Integer, Integer> lengths = new HashMap<>();
        HashSet<Integer> usedVertices = new HashSet<>();
        int sum = 0;
        usedVertices.add(0);
        int vertex = 0;
        while (usedVertices.size() < adjacencyMatrix.length) {
            for (int i = 0; i < adjacencyMatrix.length; i++) {
                if (adjacencyMatrix[vertex][i] > 0) {
                    lengths.put(i, adjacencyMatrix[vertex][i]);
                }
            }
            int min = Integer.MAX_VALUE;
            vertex = 0;
            for (int i = 0; i < adjacencyMatrix.length; i++) {
                if (lengths.containsKey(i) && !usedVertices.contains(i) && lengths.get(i) < min) {
                    min = lengths.get(i);
                    vertex = i;
                }
            }
            sum += min;
            lengths.remove(vertex);
            usedVertices.add(vertex);
        }
        return sum;
    }
}
