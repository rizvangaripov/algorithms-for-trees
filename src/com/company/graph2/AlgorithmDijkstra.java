package com.company.graph2;

import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Map;

public class AlgorithmDijkstra {
    HashMap<Integer, Integer> goDijkstra(int [][] adjacencyMatrix, int startIndex) {
        ArrayDeque<Integer> visitedVertices = new ArrayDeque<>();
        visitedVertices.addFirst(startIndex);
        HashMap<Integer, Integer> lengths = new HashMap<>();
        lengths.put(startIndex, 0);
        while (!visitedVertices.isEmpty()) {
            for (int i = 0; i < adjacencyMatrix.length; i++) {
                int length = adjacencyMatrix[startIndex][i] + lengths.get(startIndex);
                if (adjacencyMatrix[startIndex][i] > 0 &&
                        lengths.getOrDefault(i, Integer.MAX_VALUE) > length) {
                    visitedVertices.offerLast(i);
                    lengths.put(i, length);
                }
            }
            visitedVertices.pollFirst();
            if (visitedVertices.peekFirst() != null) {
                startIndex = visitedVertices.getFirst();
            }
        }
        return lengths;
    }

}